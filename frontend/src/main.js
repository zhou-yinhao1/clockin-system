import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'element-ui/lib/theme-chalk/index.css';
// import {createApp} from 'vue'
import ElementUI from 'element-ui'
import YearCalendar from 'vue-material-year-calendar'
Vue.use(YearCalendar);
Vue.use(ElementUI);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

// URLSearchParams.prototype.appendIfExists = function (key, value) {
//   if (value !== null && value !== undefined) {
//       this.append(key, value)
//   }
// };

// createApp(App).use(router).mount('#app');