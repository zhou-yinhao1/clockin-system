import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/Login.vue'
import UserHome from '@/views/System/UserHome.vue'
import UserInfo from '@/views/System/UserInfo.vue'
import ClockIn from '@/views/System/ClockIn.vue'
import Map from '@/views/System/Map.vue'
// import layoutHeaderAside from '@/layout/header-aside'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    name: 'UserHome',
    component: UserHome
  },
  {
    path: '/UserHome',
    name: 'UserHome',
    component: UserHome
  }, 
  {
    path: '/UserInfo',
    name: 'UserInfo',
    component: UserInfo,
    meta:{
      requiresAuth: true
    }
  },
  {
    path: '/ClockIn',
    name: 'ClockIn',
    component: ClockIn,
    meta:{
      requiresAuth: true
    }
  },
  {
    path: '/Map',
    name: 'Map',
    component: Map
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// 全局拦截 (守卫)

router.beforeEach((to, from, next) => {
  console.log(to)
  console.log(to)
  console.log('11111111', localStorage.getItem('token'))

  if (to.meta.requiresAuth) {
    // 判断 本地存储中是否token
    if (localStorage.getItem('access.MySystem')) {
      next()
    } else {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    }
  } else {
    next()
  }
})
export default router
