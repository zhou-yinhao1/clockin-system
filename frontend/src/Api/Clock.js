import API from '../../http'

//打卡界面相关

const postClockTime = () =>
  API({
    url: '',
    method: 'post'
  })

const getClockWeek = () =>
  API({
    url: '',
    method: 'get'
  })

const getClockMonth = () =>
  API({
    url: '',
    method: 'get'
  })

const Login = () =>
  API({
    url: '/api/UserRegister',
    method: 'post'
  })

const userInfo = (username) =>
  API({
    url: `/api/UserInfo/${username}/info/`,
    method: 'get'
  })

export default {
  postClockTime,
  getClockWeek,
  getClockMonth,
  Login,
  userInfo
}