/* 封装axios */
import axios from 'axios'
// import qs from 'qs'

/* 根据当前网址路由判断是 本地 */
// const url = document.domain
// console.log('当前服务器',url);

// this.$axios.defaults.baseURL = 'http://localhost:8000';

// 创建axios实例
const service = axios.create({
  // timeout: 5000 // 请求超时时间
})
service.defaults.withCredentials = true
// 请求拦截器
service.interceptors.request.use(
  async config => {
// <<<<<<< HEAD
//       // 获取指定名称的cookie的值
//     //   if (localStorage.getExpire('access')) {
//     //       config.headers.common['Authorization'] = `Bearer ${localStorage.getExpire('access')}`
//     //   }else{
//     //       // await getAccess()
//     //       config.headers.common['Authorization'] = `Bearer ${localStorage.getExpire('access')}`
//     //   }

//       // config.headers.common['X-CSRFToken'] = getCookie('csrftoken')
// =======
// >>>>>>> 351db83bbdd90c43af50d74e042126a39e293ed5
      return config
  },
  // error => {
  //     // 处理错误请求
  //     // Promise.reject(error)
  // }
)
// 响应拦截器
service.interceptors.response.use(
  response => {
      if (response.status === 200 || response.status === 201) {
          // 如果返回的状态码为200，说明接口请求成功，可以正常拿到数据
          // 否则的话抛出错误
          return Promise.resolve(response);
      } else {
          return Promise.reject(response);
      }
  },
  // /* 错误代码待定后期讨论  注释地方修改成弹出框提示 */
  error => {
      return Promise.reject(error.response);
  }
)


export default service;