from django.urls import path,re_path
from . import views

urlpatterns = [
    path("<int:pk>/",views.User_clockinViewSet.as_view()), #打卡信息接口
    path("dayclockin/",views.User_DayClockinViewSet.as_view()), #日常打卡接口
    path('cardtime/<int:pk>/',views.User_CardViewSet.as_view()), #打卡次数接口 
    path('export/',views.ClockinExport.as_view()), #打卡信息导出
]