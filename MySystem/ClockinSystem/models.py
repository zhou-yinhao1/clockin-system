from django.db import models
from User_Info.models import User


class ClockinStatistic(models.Model):
    User = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='user'
    )

    late_time = models.IntegerField(default=0,verbose_name="迟到次数")
    LeaEarly_time = models.IntegerField(default=0,verbose_name="早退次数")
    signin_time = models.IntegerField(default=0,verbose_name="签到次数")
    signout_time = models.IntegerField(default=0,verbose_name="签退次数")
    unsignin_time = models.IntegerField(default=0,verbose_name="未签到次数")
    unsignout_time = models.IntegerField(default=0,verbose_name="为签到次数")
    work_day = models.IntegerField(default=0,verbose_name="工作天数")
    real_day = models.IntegerField(default=0,verbose_name="真实工作天数")
    total_time = models.DecimalField(verbose_name='总工时',max_digits=4,decimal_places=2,default=0)
    average_time = models.DecimalField(verbose_name='平均工时',max_digits=4,decimal_places=2,default=0)

    class Meta:
        db_table = 'tb_clockin'
        verbose_name = '打卡'
        verbose_name_plural = verbose_name

class Day_clockin(models.Model):
    """每日打卡详情"""
    User = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='user2'
    )
    On_time = models.CharField(max_length=20,verbose_name="上班时间")
    off_time = models.CharField(max_length=20,verbose_name="下班时间")
    address = models.CharField(max_length=20,verbose_name="打卡地址",default=None)

    class Meta:
        db_table = 'tb_dayclockin'
        verbose_name = '每日打卡'
        verbose_name_plural = verbose_name

class Clockin_Card(models.Model):
    """补卡次数"""
    User = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='user1'
    )

    Card_time = models.IntegerField(default=3,verbose_name="打卡次数")

    class Meta:
        db_table = 'tb_Cardtime'
        verbose_name = '每日打卡'
        verbose_name_plural = verbose_name



# Create your models here.
