from django.shortcuts import render
from rest_framework import serializers
from .models import ClockinStatistic,Day_clockin,Clockin_Card
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly,IsAuthenticated
from User_Info.permissions import IsSelfOrReadOnly
from django.http import Http404
# from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView
from django.http import Http404
from rest_framework.response import Response
from django.db.models import F
from openpyxl import Workbook
from io import BytesIO

class User_clockinSerializer(serializers.ModelSerializer):
    """打卡详细信息-序列化器"""
    class Meta:
        model = ClockinStatistic
        fields = "__all__"
        # 前端发送数据时一定要带用户的key值
        extra_kwargs={
            'id':{
                "read_only":True,
            },
            "User_id":{
                "read_only":True,
            },
        }

class User_clockinViewSet(APIView):
    permission_classes = [IsAuthenticated]
    """打卡信息视图"""
    def get_user(self,pk):
        """获取单个用户对象"""
        try:    
            return ClockinStatistic.objects.get(User_id=pk)
        except:
            """如果该用户没有打卡信息,则创建一个"""
            ClockinStatistic.objects.create(User_id=pk)
            Clockin_Card.objects.create(User_id=pk)
            # return Http404

    def get(self,request,pk):
        """获取单个用户的打卡信息"""
        user = self.get_user(pk)
        serializer = User_clockinSerializer(user)
        return Response(serializer.data)

    def put(self,request,pk):
        """更新打卡数据"""
        ClockinStatistic.objects.filter(User_id=pk).update(late_time=F('late_time')+request.data['late_time'])
        ClockinStatistic.objects.filter(User_id=pk).update(LeaEarly_time=F('LeaEarly_time')+request.data['LeaEarly_time'])
        ClockinStatistic.objects.filter(User_id=pk).update(signin_time=F('signin_time')+request.data['signin_time'])
        ClockinStatistic.objects.filter(User_id=pk).update(signout_time=F('signout_time')+request.data['signout_time'])
        ClockinStatistic.objects.filter(User_id=pk).update(unsignin_time=F('unsignin_time')+request.data['unsignin_time'])
        ClockinStatistic.objects.filter(User_id=pk).update(unsignout_time=F('unsignout_time')+request.data['unsignout_time'])
        ClockinStatistic.objects.filter(User_id=pk).update(real_day=F('real_day')+request.data['real_day'])
        ClockinStatistic.objects.filter(User_id=pk).update(total_time=F('total_time')+request.data['total_time'])
        rl_day = ClockinStatistic.objects.get(User_id=pk).real_day
        tt_time = ClockinStatistic.objects.get(User_id=pk).total_time
        """平均每日工作时间 总时间/真实工作天数"""
        #前端不用传average_time,后端会计算出来
        ClockinStatistic.objects.filter(User_id=pk).update(average_time=tt_time/rl_day)
        user = self.get_user(pk)
        serializer = User_clockinSerializer(user)
        
        return Response(serializer.data)


class User_CardSerializer(serializers.ModelSerializer):
    """用户打卡次数"""
    class Meta:
        model = Clockin_Card
        fields = "__all__"

class User_CardViewSet(APIView):
    permission_classes = [IsAuthenticated]
    """打卡信息视图"""
    def get_user(self,pk):
        """获取单个用户对象"""
        try:    
            return Clockin_Card.objects.get(User_id=pk)
        except:
            return Http404
    
    def get(self,request,pk):
        """获取单个用户的补卡"""
        user = self.get_user(pk)
        serializer = User_CardSerializer(user)
        return Response(serializer.data)

    def put(self,request,pk):
        """更新补卡信息"""
        Clockin_Card.objects.filter(User_id=pk).update(Card_time=F('Card_time')-request.data['Card_time'])
        user = self.get_user(pk)
        serializer = User_CardSerializer(user)
        return Response(serializer.data)


"""管理员视图"""
class User_DayClockinSerializer(serializers.ModelSerializer):
    """用户每日打卡"""
    class Meta:
        model = Day_clockin
        fields = '__all__'

class User_DayClockinViewSet(ListAPIView):
    """每日打卡情况视图"""
    queryset=Day_clockin.objects.all()
    serializer_class = User_DayClockinSerializer

class ClockinExport(APIView):
# 用户打卡信息导出
    def get(slef,request):
        import urlquote
        from django.http import HttpResponse
        import datetime
        wb = Workbook()
        wb.encoding = 'utf-8'
        sheel1 = wb.active
        sheel1.title = '用户信息'
        row_one = ['用户','迟到次数','早退次数','签到次数','签退次数','未签到次数','未签退次数','工作时间'
        ,'真实工作时间','总工作时长','平均每天工作时长']
        for i in range(1,len(row_one)+1):
            sheel1.cell(row=1,column=i).value=row_one[i-1]
        all_obj = ClockinStatistic.objects.all()
        for obj in all_obj:
            max_row = sheel1.max_row+1
            obj_info = [str(obj.User),obj.late_time,obj.LeaEarly_time,obj.signin_time,obj.signout_time,
            obj.unsignin_time,obj.unsignout_time,obj.work_day,obj.real_day,obj.total_time,obj.average_time]
            for x in range(1,len(obj_info)+1):
                sheel1.cell(row=max_row,column=x).value=obj_info[x-1]
        output = BytesIO()
        wb.save((output))
        output.seek(0)
        response =HttpResponse(output.getvalue(),content_type='application/vnd.ms-excel')
        ctime_1 = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        file_name = '用户信息%s.xls' % ctime_1
        # file_name = urlquote(file_name)
        response['Content-Disposition'] = 'attachment;filename=%s' % file_name
        return response


# Create your views here.
