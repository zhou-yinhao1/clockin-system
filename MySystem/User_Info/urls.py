from django.urls import path,re_path
# from . import views
from . import views

urlpatterns = [
    path("register/",views.UserRegisterViewSet.as_view()), #注册接口
    path("userinfo/",views.UserInfoViewSet.as_view()), #用户信息全部接口
    path('myinfo/<int:pk>/',views.selfInfoViewSet.as_view()), #用户个人接口 
    path('userinfo/export/',views.UserInfoExport.as_view()) #用户数据导出
]