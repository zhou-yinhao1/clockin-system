from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.views import APIView
from rest_framework.response import Response
from User_Info.models import User
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated,IsAdminUser
from rest_framework.generics import ListAPIView,CreateAPIView,UpdateAPIView,GenericAPIView
from django.http import Http404
from rest_framework import status
from openpyxl import Workbook
from io import BytesIO

# 注册api序列化器
class UserRegisterSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'password',
            'email',
        ]
        extra_kwargs={
            'id':{
                "read_only":True,
            },
            "username":{
                "write_only":True,
            },
            "email":{
                "write_only":True,
            },
            "password":{
                "write_only":True
            }
        }

    def create(self, validated_data):
        # user = User.objects.create_user(**validated_data)
        # 调用ModelSerialzer内置的添加数据功能
        user = super().create(validated_data)
        # 针对密码要加密
        user.set_password(user.password)
        # 保存
        user.save()
        return user
    
#注册视图
class UserRegisterViewSet(CreateAPIView):
    """用户注册视图"""
    queryset = User.objects.all()
    serializer_class = UserRegisterSerializer
            
# 个人用户信息序列化
class selfInfoSerializer(serializers.ModelSerializer):
    # 个人用户信息
    class Meta:
        model =User
        fields = [
            'id',
            'username',
            'email',
            'name',
            'address_now',
            'age',
            'com_address',
            'sex',
            'dept', 
        ]

# 个人用户信息视图      
class selfInfoViewSet(APIView):
    """用户权限设置"""
    permission_classes = [IsAuthenticated]
    
    def get_user(self,pk):
        """获取单个用户对象"""
        try:
            return User.objects.get(id=pk)
        except:
            return Http404
    
    def get(self,request,pk):
        # 获取用户信息
        user = self.get_user(pk)
        serializer = selfInfoSerializer(user)
        return Response(serializer.data)

    def put(self,request,pk):
        # 更新用户信息
        user = self.get_user(pk)
        serializer = UserInfoSerializer(user,data=request.data)
        print(request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

# 管理员管理用户信息序列化
class UserInfoSerializer(serializers.ModelSerializer):
    # 后台用户信息
    class Meta:
        model =User
        fields = [
            'id',
            'username',
            'email',
            'name',
            'address_now',
            'age',
            'com_address',
            'sex',
            'dept', 
        ]

# 后台用户信息视图
class UserInfoViewSet(ListAPIView):
    permission_classes=[IsAdminUser]
    queryset = User.objects.all()
    serializer_class = UserInfoSerializer



class UserInfoExport(APIView):
# 用户信息导出
    def get(slef,request):
        import urlquote
        from django.http import HttpResponse
        import datetime
        wb = Workbook()
        wb.encoding = 'utf-8'
        sheel1 = wb.active
        sheel1.title = '用户信息'
        row_one = ['姓名','用户名','性别','手机号码','所属部门','家庭住址','联系地址','职位']
        for i in range(1,len(row_one)+1):
            sheel1.cell(row=1,column=i).value=row_one[i-1]
        all_obj = User.objects.all()
        for obj in all_obj:
            max_row = sheel1.max_row+1
            obj_info = [obj.name,obj.username,obj.sex,obj.mobile,obj.dept,obj.address_now,obj.com_address,obj.position]
            for x in range(1,len(obj_info)+1):
                sheel1.cell(row=max_row,column=x).value=obj_info[x-1]
        output = BytesIO()
        wb.save((output))
        output.seek(0)
        response =HttpResponse(output.getvalue(),content_type='application/vnd.ms-excel')
        ctime_1 = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        file_name = '用户信息%s.xls' % ctime_1
        # file_name = urlquote(file_name)
        response['Content-Disposition'] = 'attachment;filename=%s' % file_name
        return response







