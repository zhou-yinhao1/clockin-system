# 导入
from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    """
    我们重写用户模型类, 继承自 AbstractUser
    """
    mobile = models.CharField(max_length=11, unique=True, verbose_name='手机号',null=True,blank=True)
    age = models.IntegerField(default=0,verbose_name="年龄",null=True,blank=True)
    name = models.CharField(max_length=30,unique=True,verbose_name="姓名",default=None,null=True,blank=True)
    address_now = models.CharField(max_length=20,unique=True,verbose_name="现居住地",default=None,null=True,blank=True)
    com_address = models.CharField(max_length=20,unique=True,verbose_name='联系地址',default=None,null=True,blank=True)
    
    SEX_OPTION=(
        (1,"男"),
        (2,"女"),
    )
    sex = models.CharField(max_length=5,choices=SEX_OPTION,default=1,verbose_name="性别")

    DEPT_OPTION=(
        (1,"技术部"),
        (2,'销售部'),
        (3,'人事部'),
        (4,'行政部'),
        (5,'财务部')
    )
    dept = models.CharField(max_length=5,choices=DEPT_OPTION,default=1,verbose_name="部门")

    position = models.CharField(max_length=10,verbose_name="职位",default=None,null=True,blank=True)


    class Meta:
        db_table = 'tb_users'
        verbose_name = '用户'
        verbose_name_plural = verbose_name
    
    def __str__(self):
        return self.username